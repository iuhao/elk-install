echo "Installing elk ....."

ip_address=`ifconfig ens33 | grep "inet " | awk '{print $2}'`
# read -p "你的当前ip地址是$ip_address,确认使用该ip?y or n：" var
# if [ $var == 'n' ]
# then
	# read -p "请输入新的ip地址：" var1
	# ip_address=$var1
# else
	# echo "y" >> /dev/null
# fi

kill -9 `netstat -tlnp | grep ':::6379' | awk '{print $7}' | awk 'BEGIN{FS="/"} {print $1}'`
kill -9 `netstat -tlnp | grep ':::9300' | awk '{print $7}' | awk 'BEGIN{FS="/"} {print $1}'`
kill -9 `netstat -tlnp | grep ':::9301' | awk '{print $7}' | awk 'BEGIN{FS="/"} {print $1}'`
kill -9 `netstat -tlnp | grep '0.0.0.0:5601' | awk '{print $7}' | awk 'BEGIN{FS="/"} {print $1}'`

rm -rf /usr/local/redis
rm -rf elasticsearch-1.7.2
rm -rf kibana-4.1.2-linux-x64
rm -rf logstash-1.5.4

tar xf redis-2.8.19.tar.gz
cd redis-2.8.19
make 
make PREFIX=/usr/local/redis install
rm -rf /usr/local/redis/redis.conf
cp redis.conf /usr/local/redis/
sed -i "s/daemonize no/daemonize yes/g" /usr/local/redis/redis.conf
/usr/local/redis/bin/redis-server /usr/local/redis/redis.conf

cd ..
tar xf elasticsearch-1.7.2.tar.gz
echo 'network.host: 0.0.0.0' >> elasticsearch-1.7.2/config/elasticsearch.yml
./elasticsearch-1.7.2/bin/plugin install lmenezes/elasticsearch-kopf
./elasticsearch-1.7.2/bin/plugin install mobz/elasticsearch-head
./elasticsearch-1.7.2/bin/elasticsearch -d

tar xf logstash-1.5.4.tar.gz
tar xf kibana-4.1.2-linux-x64.tar.gz

sed -i "s/192.168.1.130/$ip_address/g" logstash-redis.conf
sed -i "s/localhost:9200/$ip_address:9200/g" kibana-4.1.2-linux-x64/config/kibana.yml

yum -y install nohup
nohup ./logstash-1.5.4/bin/logstash agent -f logstash-redis.conf &
nohup ./kibana-4.1.2-linux-x64/bin/kibana &
/usr/local/redis/bin/redis-cli rpush logstash:redis test
echo "Install Successful,please visit http://$ip_address:5601"